<?php
require_once CLASSES.DS.'RestCurlClient.php';
class VendorModel {
  public function construct(){}
  public function listAll(){
    $api=New RestCurlClient();
    try {
      set_time_limit(0);
      //echo URL_APIBASE.'/vendor';
      $response=$api->get(URL_APIBASE.'/vendor');
      //print_r($response);
      if ($response) return $response;
      else return false;
    } catch (Exception $e) {
      //Que fait-on en fonction du code erreur?  #TODO#
      return false;
    }
  }
  public function listOne($id){
    $api=New RestCurlClient();
    try {
      set_time_limit(0);
      $response=$api->get(URL_APIBASE.'/vendor/'.$id);
      if ($response) return $response;
      else return false;
    } catch (Exception $e) {
      //Que fait-on en fonction du code erreur?  #TODO#
      return false;
    }
  }
  public function add($Nom,$AccountNumber,$CreditRating,$PreferredVendorStatus,$ActiveFlag){
    $api=New RestCurlClient();
    try {
      $p=array(
        'Nom'   => $Nom,
        'AccountNumber'=>$AccountNumber,
        'CreditRating'=>$CreditRating,
        'PreferredVendorStatus'=>$PreferredVendorStatus,
        'ActiveFlag'=>$ActiveFlag
      );
      //set_time_limit(0);
      
      //$response=$api->post(URL_APIBASE.'/vendor/',null,$p);
      $response=$api->post(URL_APIBASE.'/vendor/',null,$p);
      if ($response) 
      {

        return $response;
      }

      else return false;
    } catch (Exception $e) {
      //Que fait-on en fonction du code erreur?  #TODO#
      return false;
    }
  }
  public function delete($id){
    $api=New RestCurlClient();
    try {
      set_time_limit(0);
      $response=$api->delete(URL_APIBASE.'/vendor/'.$id);
      if ($response) return $response;
      else return false;
    } catch (Exception $e) {
      //Que fait-on en fonction du code erreur?  #TODO#
      return false;
    }
  }
}
?>