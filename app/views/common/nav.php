<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="#">UHA MIAGE/IM</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo URL_BASE.'/';?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo URL_BASE.'/employee/listall';?>">Employés</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo URL_BASE.'/employee/listmanagers';?>">Managers</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo URL_BASE.'/department/listall';?>">Départements</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo URL_BASE.'/vendor/listall';?>">Fournisseurs</a>
      </li>
    </ul>
  </div>
</nav>