<?php
/*
 	1 	VendorID  Primary 	int(11)
	2 	AccountNumber 	varchar(15) 	utf8_general_ci
	3 	Name 	varchar(50) 	utf8_general_ci
	4 	CreditRating 	tinyint(4)
	5 	PreferredVendorStatus 	bit(1)
	6 	ActiveFlag 	bit(1)
	7 	PurchasingWebServiceURL 	mediumtext 	utf8_general_ci
	8 	ModifiedDate 	timestamp
*/
require_once CLASSES.DS.'modelpdo.php';
class VendorModel extends ModelPDO{
  public function construct(){}
  public function listAll(){
    $sql='SELECT V.VendorID,C.ContactID, C.FirstName, C.MiddleName, V.Name, V.CreditRating ,
    V.AccountNumber,V.CreditRating,V.PreferredVendorStatus
    from Vendor as V
    inner join VendorContact as VC 
     on VC.VendorID=V.VendorID 
     inner join Contact as C 
     on C.ContactID=VC.ContactID 
     WHERE V.ActiveFlag <>0';
    return $this->select($sql);
  }
  public function listOne($id){
    $sql='SELECT V.VendorID,C.ContactID, C.FirstName, C.MiddleName, V.Name, V.CreditRating ,
    V.AccountNumber,V.CreditRating,V.PreferredVendorStatus
    from Vendor as V
    inner join VendorContact as VC 
     on VC.VendorID=V.VendorID 
     inner join Contact as C 
     on C.ContactID=VC.ContactID 
     WHERE V.ActiveFlag <>0 and V.VendorID=:id';
     $p=array(
      ':id'   => array('value'=>$id, 'type'=>PDO::PARAM_INT)
    );
    return current($this->select($sql,$p));
  }
  public function add($content){
    //$content is a format json
    $sql='INSERT INTO `vendor` (`VendorID`, `AccountNumber`, `Name`, `CreditRating`, `PreferredVendorStatus`,`ActiveFlag`,`ModifiedDate`) 
    VALUES (NULL,:AccountNumber,:Nom,:CreditRating,:PreferredVendorStatus,:ActiveFlag, current_timestamp())';
    $obj=json_decode($content);
     $p=array(
      ':AccountNumber'   => array('value'=>$obj->AccountNumber, 'type'=>PDO::PARAM_STR),
      ':Nom'   => array('value'=>$obj->Nom, 'type'=>PDO::PARAM_STR),
      ':CreditRating'   => array('value'=>$obj->CreditRating, 'type'=>PDO::PARAM_INT),
      ':PreferredVendorStatus'   => array('value'=>$obj->PreferredVendorStatus, 'type'=>PDO::PARAM_INT),
      ':ActiveFlag'   => array('value'=>$obj->ActiveFlag, 'type'=>PDO::PARAM_INT)
    );
      // require_once CLASSES.DS.'view.php';
      // $v=new View();
      // $v->setVar('data',$this->insert($sql,$p));
      // $v->renderjson(200);
    return $this->insert($sql,$p);
  }
  public function remove($id){
    $sql='delete from vendor where VendorID=:id';
    $p=array(
      ':id'   => array('value'=>$id, 'type'=>PDO::PARAM_INT)
    );
    return $this->delete($sql,$p);
  }
}
?>
